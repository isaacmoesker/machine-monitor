# Used in ConfigWebsite and ConfigDNS
Function local:PromptYesNo($local:message) {
    $local:choice = (Read-Host -prompt "`n$local:message (y/n)").ToLower();
    If ($local:choice -eq "y" -or $local:choice -eq "yes") {
        return $true;
    } ElseIf ($local:choice -eq "n" -or $local:choice -eq "no") {
        return $false;
    } Else {
        Write-Host "Yes or no please...`n";
        return PromptYesNo $local:message;
    }
}

# Used in ConfigWebsite and ConfigDNS
Function local:PromptInt($local:message, $local:items) {
    Write-Host "`n---------- MENU ----------";
    For ($local:i = 0; $local:i -lt $local:items.Count; $local:i++) {
        Write-Host ("$local:i`t" + $local:items[$local:i]);
    }
    Write-Host "--------------------------`n";

    Do {
        Try {
            [int]$local:choice = Read-Host -prompt $local:message;
            Break;
        } Catch {
            Write-Host "Please enter a 32 bit integer.";
        }
    } While ($true)

    If ($local:choice -lt $local:items.Count -and $local:choice -ge 0) {
        return $local:choice;
    } Else {
        Write-Host "`n$local:choice is not an option...";
        return PromptInt $local:message $local:items;
    }
}

# Test operating system
Function TestOS ($scriptDir, $preferedOS = "Microsoft Windows Server 2012 R2 Standard") {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestOS";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Get operating system";
        Try {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Get operating system using Get-CimInstance";
            $os = (Get-CimInstance Win32_OperatingSystem).Caption;
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
        } Catch {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_. Using Get-WmiObject";
            $os = (Get-WmiObject Win32_OperatingSystem).Caption;
        }
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestOS";

        If ($os -eq $preferedOS) { return $true; }
        Else {
            $continue? = PromptYesNo "Machin Monitor is made for $preferedOS`nWould you like to continue? (y/n)";
            if ($continue?) {
                return $true;
            } Else {
                return $false;
            }
        }
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestOS";
        return $false;
    }
}

# Test network connection
Function TestNetwork ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestNetwork";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Ping the local machine";
        $test = (Test-NetConnection).PingSucceeded;
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        return $test;
    }
    Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestNetwork";
        return $false;
    }
}

# Test for a static IP address
Function TestStaticIP ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestNetwork";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Check for static IP";
        $query = "SELECT IPAddress, DefaultIPGateway FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled=TRUE AND DHCPEnabled=FALSE";
        Try {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Check for static IP using Get-CimInstance";
            $ip = (Get-CimInstance -ErrorAction SilentlyContinue -Query $query -ErrorAction Stop).IPAddress;
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
        } Catch {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_. Using Get-WmiObject";
            $ip = (Get-WmiObject -ErrorAction SilentlyContinue -Query $query).IPAddress;
        }
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestNetwork";

        If ([string]::IsNullOrEmpty($ip)) { return $false; }
        Else { return $true; }
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestNetwork";
        return $false;
    }
}

# Test the local computer to be a domain member
Function TestDomain ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestDomain";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Check that the local machine is a member of a domain";
        Try {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Check that the local machine is a member of a domain using Get-CimInstance";
            $domainMember = (Get-CimInstance Win32_ComputerSystem).PartOfDomain;
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
        } Catch {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_. Using Get-WmiObject";
            $domainMember = (Get-WmiObject Win32_ComputerSystem).PartOfDomain;
        }
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestDomain";

        return $domainMember;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestDomain";
        return $false;
    }
}

# Test script is running elevated
Function TestElevated ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BEGIN] TestElevated";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BLOCK 1][TRY] Check running as domain admin and elevated";
        $wid = [System.Security.Principal.WindowsIdentity]::GetCurrent();
        $prp = new-object System.Security.Principal.WindowsPrincipal($wid);
        $adm = [System.Security.Principal.WindowsBuiltInRole]::Administrator;
        $IsAdmin = $prp.IsInRole($adm);

        #$domain = Get-ADDomain | Select-Object -ExpandProperty Name;
        #$admins = Get-ADGroupMember "Domain Admins" | Select-Object -ExpandProperty Name;

        # Check elevated
        If ($IsAdmin) {
            # Check domain administrator
            #ForEach ($admin in $admins) {
                #If ("$domain\$admin" -eq $prp.Identities.Name) {
                    Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BLOCK 1][TRY] Success";
                    Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[END] TestElevated";
                    return $true;
                #}
            #}
        }

        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BLOCK 1][TRY] Not running elevated or as a domain admin";
        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[END] TestElevated";
        return $false;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[END] TestElevated";
        return $false;
    }
}

# Test script is running as a domain administrator
Function TestDomainAdmin ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BEGIN] TestDomainAdmin";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BLOCK 1][TRY] Check running as domain admin";

        $admins = Get-ADGroupMember "Domain Admins" | Select-Object -ExpandProperty Name;

        # Check domain administrator
        ForEach ($admin in $admins) {
            If ("$env:USERDOMAIN\$admin".ToLower() -eq "$env:USERDOMAIN\$env:USERNAME".ToLower()) {
                Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BLOCK 1][TRY] Success";
                Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[END] TestDomainAdmin";
                return $true;
            }
        }

        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[BLOCK 1][TRY] Not as a domain admin";
        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[END] TestDomainAdmin";
        return $false;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -ErrorAction SilentlyContinue -Value "[END] TestDomainAdmin";
        return $false;
    }
}

# Test required software is installed
Function TestSoftware ($scriptDir, $software) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestSoftware";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Searching for $software";
        $query = "SELECT Name FROM Win32_Product WHERE Name LIKE '%$software%'";
        Try {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Searching for $software using Get-CimInstance";
            $installed = Get-CimInstance -ErrorAction Stop -Query $query;
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
        } Catch {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_. Using Get-WmiObject";
            $installed = Get-WmiObject -ErrorAction Stop -Query $query;
        }
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestSoftware";

        If ([string]::IsNullOrEmpty($installed)) { return $false; }
        Else { return $true; }
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestSoftware";
        return $false;
    }
}

# Test the local computer to be a domain member
Function TestPHP ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestPHP";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Search for a PHP instalation using Get-Command";
        Get-Command php -errorAction Stop | Out-Null;
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestPHP";
        return $true;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestPHP";
        return $false;
    }
}

# Configure MySQL root password
Function ConfigMySQL ($scriptDir) {
    # try connecting to MySQL with supplied password
    # create database and table structure
    # insert password into PHP\MySQLConnect.php
    # insert password into PowerShell\MySQLConnect.ps1
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] ConfigMySQL";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Connect and build MySQL database";

        $continue? = PromptYesNo "Would you like to configure MySQL?";

        If ($continue?) {
            # MySQL connect test
            $service = Get-service *MySQL*;
            Start-Service $service -ErrorAction SilentlyContinue;
            Do {
                Try {
                    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Get MySQL root password from user and test it";
                    $password = Read-Host -prompt "`nEnter the password for your MySQL root account";
                    [void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data");
                    $ConnectionString = "server=localhost;port=3306;uid=root;pwd=$password;";
                    $Connection = New-Object MySql.Data.MySqlClient.MySqlConnection;
                    $Connection.ConnectionString = $ConnectionString;
                    $Connection.Open();
                    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
                    Break;
                } Catch {
                    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] Incorrect password";
                    Write-Host "`nIncorrect password.`n";
                }
            } While ($true)

            # Create database and table structure
            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][TRY] Create database and table structure";
                $query = Get-Content -Path "$scriptDir\Documentation\MySQL Setup.txt" | Out-String;
                $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($Query, $Connection);
                $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command);
                $DataSet = New-Object System.Data.DataSet;
                $RecordCount = $dataAdapter.Fill($dataSet, "data");
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][CATCH] $_";
            }
            $Connection.Close();

            # Change password in MySQLConnect.ps1
            $content = Get-Content -Path "$scriptDir\PowerShell\MySQLConnect.ps1" | Out-String;
            #$content = $content -replace "#.*";
            #$content = $content.trimEnd();
            $content = $content.Replace("pwd=P@ssw0rd;", "pwd=$password;");
            Set-Content -Path "$scriptDir\PowerShell\MySQLConnect.ps1" -Value $content;
            #Invoke-Expression "$scriptDir\PowerShell\SignScript.ps1 $scriptDir\PowerShell\MySQLConnect.ps1" | Out-Null;

            # Change password in MySQLConnect.php
            $content = Get-Content -Path "$scriptDir\PHP\MySQLConnect.php" | Out-String;
            $content = $content.Replace("P@ssw0rd", $password);
            Set-Content -Path "$scriptDir\PHP\MySQLConnect.php" -Value $content;
        }

        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigMySQL";
        return $true;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigMySQL";
        return $false;
    }
}

# Test administrative account
Function ConfigAdminAccount ($scriptDir) {
    # check is credentials are legit
    # check if the account is a member of the domain nadministrators group
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] TestAdminAccount";
    [hashtable]$return = @{};
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Get administrative account";
        While ($true) {
            $admins = Get-ADGroupMember "Domain Admins";
            Write-Host "`n------------- MENU -------------";
            For ($i = 0; $i -lt $admins.Length; $i++) {
                Write-Host ("$i`t" + $admins[$i].name);
            }
            Write-Host "--------------------------------`n";

            $choice = Read-Host -prompt "Select a domain administrator`nNOTE: Make sure the selected administrator is a member of the 'Log on as batch job' security group";
            If ($choice -lt $admins.Length -and $choice -gt -1) {
                $cred = Get-Credential -credential ("$env:USERDOMAIN\" + $admins[$choice].name);

                $username = $cred.username;
                $password = $cred.GetNetworkCredential().password;

                $ldap = "LDAP://" + ([ADSI]"").distinguishedName;
                $user = New-Object System.DirectoryServices.DirectoryEntry($ldap, $username, $password) -errorAction Stop;

                If ($user.name -eq $null) { Write-Host "`nIncorrect password.`n"; }
                Else { Break; }
            } Else {
                Write-Host "`n$choice is not an option...`n";
            }
        }
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";

        $return.success = $true; 
        $return.cred = $cred;
        return $return;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] TestAdminAccount";

        $return.success = $false;
        $return.cred = $null;
        return $return;
    }
}

# Configure IIS application pool
Function ConfigAppPool ($scriptDir, $cred) {
    # create application pool in IIS
    # set identity to administrative account
    # start application pool
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] ConfigAppPool";

    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Create IIS application pool using New-WebAppPool";
        New-WebAppPool -Name "Machine Monitor" -ErrorAction SilentlyContinue | Out-Null;
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
    } Catch { 
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
    }

    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Set application pool identity to administative account";
        $username = $cred.UserName;
        $password = $cred.GetNetworkCredential().password;
        Set-ItemProperty "IIS:\AppPools\Machine Monitor" -name processModel `
            -value @{userName=$username;password=$password;identitytype=3} -ErrorAction Stop;
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigAppPool";
        return $false; 
    }
    
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigAppPool";

    Start-WebAppPool "Machine Monitor" -ErrorAction SilentlyContinue;
    return $true;
}

# Configure web site
Function ConfigWebSite ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] ConfigWebSite";
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Create and configure IIS website";
        $continue? = PromptYesNo "Would you like to create an IIS website?";

        If ($continue?) {
            Do {
                # Prompt for website directory
                $path = Read-Host -Prompt "`nEnter a path for the IIS web site (Eg. C:\inetpub)";
                If (Test-Path $path) {
                    # Move files into directory and create website
                    Try {
                        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Copy files into the new directory and create the web site";
                        Copy-Item -Path $scriptDir -Destination "$Path\Machine Monitor" -Recurse -ErrorAction Stop;

                        Do {
                            $port = Read-Host -Prompt "`nEnter a port for the IIS web site (Eg. 80)`nNOTE: If you plan on adding a DNS record, we recomend port 80";

                            # Get all used ports
                            $sites = Get-ChildItem -Path IIS:\Sites;
                            $arr = $sites.Bindings.Collection.bindingInformation -Split ":";
                            $ports = @();
                            For ($i = 1; $i -lt $arr.Length - 1; $i = $i + 3) {
                                $ports += ($arr[$i]);
                            }

                            If (!($ports -Contains $port)) {
                                New-WebSite -Name "Machine Monitor" -Port $port -ApplicationPool "Machine Monitor" `
                                    -PhysicalPath "$Path\Machine Monitor" -ErrorAction Stop | Out-Null;
                                Break;
                            } Else {
                                Write-Host "`nPort is in use.";
                            }
                        } While ($true)
                        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
                    } Catch {
                        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_";
                    }

                    # Add mime type to web site
                    $mime = (Get-WebConfiguration /system.webServer/staticContent -PSPath "IIS:\Sites\Machine Monitor").collection `
                        | Where-Object { $_.fileExtension -eq ".woff2" };

                    If ($mime.Count -lt 1) {
                        Add-WebConfigurationProperty -PSPath "IIS:\Sites\Machine Monitor" `
                            -Filter /system.webServer/staticContent `
                            -Name "." `
                            -Value @{fileExtension='.woff2';mimeType='font/woff2'};
                    }
                
                    Try {
                        # Add handler mapping to web site
                        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][TRY] Add FastCGI IIS handler for PHP";
                        
                        Set-WebConfiguration -Filter "/System.webServer/handlers/*" `
                            -Metadata overrideMode `
                            -Value Allow `
                            -PSPath "IIS:\";

                        $phpcgi = Get-WebHandler -PSPath "IIS:\Sites\Machine Monitor" | `
                            Where-Object { $_.ScriptProcessor -like "*php-cgi.exe" };

                        If ($phpcgi.Count -lt 1) {
                            $phpPath = Read-Host -Prompt "`tEnter a path to php-cgi.exe (Eg. C:\Program Files (x86)\PHP\v5.6\php-cgi.exe)";
                            New-WebHandler -Name "PHP_via_FastCGI" `
                                -PSPath "IIS:\Sites\Machine Monitor" `
                                -ScriptProcessor $phpPath `
                                -Modules "FastCgiModule" `
                                -Path "*.php" `
                                -Verb "*";
                        }
                        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][TRY] Success";
                    } Catch {
                        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][CATCH] $_";
                    }
                    Break;
                } Else {
                    Write-Host "`n'$path' is not a valid path.`n";
                }
            } While ($true)

            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 4][TRY] Toggle authentication modes for IIS website";
                # Disable all authentications
                Set-WebConfiguration -Filter "/System.webServer/security/authentication/*" `
                    -Metadata overrideMode `
                    -Value Allow `
                    -PSPath "IIS:\";

                $auths = Get-WebConfiguration `
                    -filter "/system.webServer/security/authentication/*" `
                    -PSPath "IIS:\Sites\Machine Monitor" | Select-Object -ExpandProperty ItemXPath;

                ForEach ($auth in $auths) {
                    Set-WebConfiguration -Filter $auth `
                        -metadata overrideMode `
                        -Value Allow `
                        -PSPath "IIS:\";
                    Set-WebConfigurationProperty -Filter $auth `
                        -PSPath "IIS:\Sites\Machine Monitor" `
                        -Name Enabled `
                        -Value False;
                }

                # Enable windowsAuthentication
                Set-WebConfiguration -Filter /System.WebServer/Security/Authentication/windowsAuthentication `
                    -metadata overrideMode `
                    -value Allow `
                    -PSPath "IIS:\";
                Set-WebConfigurationProperty -Filter /system.webServer/security/authentication/windowsAuthentication `
                    -PSPath "IIS:\Sites\Machine Monitor" `
                    -Name Enabled `
                    -Value True;

                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 4][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 4][CATCH] $_";
                return $false;
            }

            # Set FastCGI Protocol to Tcp
            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 5][TRY] Set FastCGI Protocol to tcp";
              
                $configs = Get-WebConfiguration -Filter "/System.webServer/fastCgi/application" -PSPath "IIS:\"

                $flag = $false
                ForEach ($config in $configs) {
                    if ($config.fullPath -eq "C:\Program Files (x86)\PHP\v5.6\php-cgi.exe") {
                        $flag = $true
                        break
                    }
                }

                if ($flag) {
                    $path = "C:\Program Files (x86)\PHP\v5.6\php-cgi.exe"
                } else {
                    $choice = PromptInt "Select a FastCGI PHP application" $configs;
                    $path = $configs[$choice].fullPath
                }

                Set-WebConfigurationProperty "/System.webServer/fastCgi/application[@fullPath='$path']" -Name protocol -Value Tcp  

                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 5][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 5][CATCH] $_";
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigWebsite";
                return $false;
            }

            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigWebSite";
            return $true;
        } Else {
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
            Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigWebSite";
            return $true;
        }
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigWebSite";
        return $false;
    }
}

# Add DNS entry for web site
Function ConfigDNS ($scriptDir) {
    Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BEGIN] ConfigDNS";
    Try {
        # Prompt for DNS record entry
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Create DNS entry for the IIS website";
        $continue? = PromptYesNo "Would you like to create a Type A DNS record?";

        # Perform DNS addition
        If ($continue?) {
            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Get all domain controllers";
                $DCs = (Get-ADDomain -ErrorAction Stop).ReplicadirectoryServers;
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 2][CATCH] $_";
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigDNS";
                return $false;
            }

            If ($DCs.Count -gt 1) {
                $choice = PromptInt "Select a domain controller" $DCs;
                $DC = $DCs[$choice];
            } Else {
                $DC = $DCs;
            }

            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][TRY] Get DNS zones";
                $zones = Get-DnsServerZone -ComputerName $DC -ErrorAction Stop | Select-Object -ExpandProperty ZoneName;
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 3][CATCH] $_";
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigDNS";
                return $false;
            }

            If ($zones.Count -gt 1) {
                $choice = PromptInt "Select a DNS server zone" $zones;
                $zone = $zones[$choice];
            } Else {
                $zone = $zones;
            }

            $hostname = Read-Host -prompt "`nEnter a hostname";
            $query = "SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled=TRUE AND DHCPEnabled=FALSE";
            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 4][TRY] Get IP address of local machine using Get-CimInstance";
                $IP = (Get-CimInstance -ErrorAction SilentlyContinue -Query $query).IPAddress[0];
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 4][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" `
                    -Value "[BLOCK 4][CATCH] $_. Using Get-WmiObject";
                $IP = (Get-WmiObject -ErrorAction SilentlyContinue -Query $query).IPAddress[0];
            }

            Try {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 5][TRY] Add DNS server resource record A";
                Add-DnsServerResourceRecordA -Name $hostname -ZoneName $zone -IPv4Address $IP -ComputerName $DC -AllowUpdateAny -ErrorAction Stop;
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 5][TRY] Success";
            } Catch {
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 5][CATCH] $_";
                Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigDNS";
                return $false;
            }
        }

        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigDNS";
        return $true;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigDNS";
        return $false;
    }
}

# Import and start scheduled tasks
Function ConfigScheduledTask ($scriptDir, $cred) {
    Try {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Create and start scheduled tasks";
        schtasks -Create -F `
            -TN "Machine Monitor\InsertMachines" `
            -XML "$scriptDir\Tasks\InsertMachines.xml" `
            -RU $cred.UserName `
            -RP $cred.GetNetworkCredential().password | Out-Null;
        schtasks -Create -F `
            -TN "Machine Monitor\InsertPing" `
            -XML "$scriptDir\Tasks\InsertPing.xml" `
            -RU $cred.UserName `
            -RP $cred.GetNetworkCredential().password | Out-Null;
        schtasks -Create -F `
            -TN "Machine Monitor\InsertEventCounts" `
            -XML "$scriptDir\Tasks\InsertEventCounts.xml" `
            -RU $cred.UserName `
            -RP $cred.GetNetworkCredential().password | Out-Null;

        Start-ScheduledTask -TaskName "Machine Monitor\InsertMachines";
        Start-ScheduledTask -TaskName "Machine Monitor\InsertPing";
        Start-ScheduledTask -TaskName "Machine Monitor\InsertEventCounts";

        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][TRY] Success";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigScheduledTask";
        return $true;
    } Catch {
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[BLOCK 1][CATCH] $_";
        Add-Content -Path "$scriptDir\SetupLog.txt" -Value "[END] ConfigScheduledTask";
        return $false;
    }
}