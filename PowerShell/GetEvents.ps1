param (
    [string]$hostname = $(throw "Please specify a hostname."),
    [byte]$level = $(throw "Please specify 1, 2, or 3.")
)

Try {
    Add-Type -AssemblyName System.Web;
    $queryxml = "<QueryList>
                    <Query Id='0' Path='Application'>
                      <Select Path='Application'>*[System[(Level=$level)]]</Select>
                    </Query>
                 </QueryList>";
    $objects = Get-WinEvent -ComputerName $hostname -FilterXml $queryxml -MaxEvents 40 -ErrorAction SilentlyContinue;
    
    $output = "<events>";
    foreach ($object in $objects)
    {
        $output += "<event>";
            $output += "<Message>" + [System.Web.HttpUtility]::HtmlEncode($object.Message) + "</Message>";
            $output += "<Id>" + [System.Web.HttpUtility]::HtmlEncode($object.Id) + "</Id>";
            $output += "<LogName>" + [System.Web.HttpUtility]::HtmlEncode($object.LogName) + "</LogName>";
            $output += "<TimeCreated>" + [System.Web.HttpUtility]::HtmlEncode($object.TimeCreated) + "</TimeCreated>";
        $output += "</event>";
    }
    $output += "</events>";
    Write-Output $output;
}

Catch {
    Write-Output "$hostname is unavailable.";
}