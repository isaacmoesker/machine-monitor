﻿Try {
    . ".\MySQLConnect.ps1";

    $machines = Get-ADComputer -Filter * -Properties ObjectGUID,IPv4Address,Name;
        
    $query = "";
    ForEach ($machine in $machines) {
        $address = $machine.IPv4Address;
        $machineid = $machine.ObjectGUID;
        $hostname = $machine.Name;

        $query += "UPDATE Machines SET address='' WHERE address='$address';
            INSERT INTO Machines (machineid,address,hostname) 
            VALUES ('$machineid','$address','$hostname') 
            ON DUPLICATE KEY UPDATE address='$address',hostname='$hostname';";
    }

    $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $Connection);
    $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command);
    $DataSet = New-Object System.Data.DataSet;
    $RecordCount = $DataAdapter.Fill($DataSet, "data");
}

Catch {
    Write-Host "ERROR : Unable to run query : $query `n$Error[0]";
}

Finally {
    $Connection.Close();
}