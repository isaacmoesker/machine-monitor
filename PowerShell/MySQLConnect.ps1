$ConnectionString = "server=localhost;port=3306;uid=root;pwd=Cl3arWb4ng0;database=Machine_Monitor";

Try {
    [void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data");
    $Connection = New-Object MySql.Data.MySqlClient.MySqlConnection;
    $Connection.ConnectionString = $ConnectionString;
    $Connection.Open();
}

Catch {
    Write-Host "ERROR: Unable to connect to MySQL: $query `n$Error[0]";
}