﻿Try {
    . ".\MySQLConnect.ps1";

    $query = "SELECT machineid,hostname FROM Machines WHERE address!='';";

    $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $Connection);
    $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command);
    $DataSet = New-Object System.Data.DataSet;
    $DataAdapter.Fill($DataSet, "data");
    $machines = $DataSet.Tables[0];

    $query = "";
    ForEach ($machine in $machines) {
        Try {
            $queryxml = "
                <QueryList>
                    <Query Id='0' Path='Application'>
                        <Select Path='Application'>*[System[(Level=1  or Level=2 or Level=3)]]</Select>
                    </Query>
                </QueryList>";
            $events = Get-WinEvent -ComputerName $machine.hostname -FilterXml $queryxml -ErrorAction SilentlyContinue;

            $countError1 = $events | Where-Object LevelDisplayName -eq "Error" | measure-object | Select-Object -ExpandProperty Count;
            $countCritical2 = $events | Where-Object LevelDisplayName -eq "Critical" | measure-object | Select-Object -ExpandProperty Count;
            $countWarning3 = $events | Where-Object LevelDisplayName -eq "Warning" | measure-object | Select-Object -ExpandProperty Count;

            $query += "INSERT INTO EventCounts (machineid,error,critical,warning) 
                VALUES ('" + $machine.machineid + "',$countError1,$countCritical2,$countWarning3) 
                ON DUPLICATE KEY UPDATE error=$countError1,critical=$countCritical2,warning=$countWarning3;";
        } Catch {}
    }
    $query;
    $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $Connection);
    $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command);
    $DataSet = New-Object System.Data.DataSet;
    $DataAdapter.Fill($DataSet, "data");
}

Catch {
    Write-Host "ERROR : Unable to run query : $query `n$Error[0]";
}

Finally {
    $Connection.Close();
}