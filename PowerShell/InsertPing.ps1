﻿function FastPing{
  [CmdletBinding()]
  param(
    [String]$computername = "10.0.0.1",
    [int]$delay = 500
  )

  $ping = new-object System.Net.NetworkInformation.Ping
  try {
    if ($ping.send($computername,$delay).status -ne "Success") {
      return 0;
    }
    else {
      return 1;
    }
  } catch {
    return 0;
  }
}

Try {
    . ".\MySQLConnect.ps1";

    $query = "SELECT machineid,hostname FROM Machines WHERE address!='';";
    $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $Connection);
    $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command);
    $DataSet = New-Object System.Data.DataSet;
    $RecordCount = $DataAdapter.Fill($DataSet, "data");

    $query = "";
    ForEach ($machine in $DataSet.Tables[0]) {
        $ping = FastPing $machine.hostname;
        $machineid = $machine.machineid;
        $query += "UPDATE Machines SET ping=$ping WHERE machineid='$machineid';";
        $query += "INSERT INTO PingLog (machineid,ping,created) VALUES ('$machineid',$ping,NOW()) ON DUPLICATE KEY UPDATE machineid='$machineid',ping=$ping,created=NOW();";
    }

    $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $Connection);
    $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command);
    $DataSet = New-Object System.Data.DataSet;
    $RecordCount = $DataAdapter.Fill($DataSet, "data");
}

Catch {
    Write-Host "ERROR : Unable to run query : $query `n$Error[0]";
}

Finally {
    $Connection.Close();
}