# Get directory of script
$scriptDir = Split-Path $script:MyInvocation.MyCommand.Path; Write-Host $scriptDir;
Set-Location -Path $scriptDir | Out-Null;

# Include relevent files
. ".\PowerShell\SetupHelper.ps1";

# Relevent functuions
Function DefaultError () {
    Write-Host "`nTest failed.`nReview $scriptDir\SetupLog.txt for additional details.`n`nExiting.`n";
    Exit;
}

# Begin testing
Write-Output "`n ----------------- `n|                 |`n| Machine Monitor |`n|                 |`n ----------------- `n";
Write-Output "`nBegin Setup`n";

# Test system requirements
Write-Output "`nSystem Requirements:`n";

$test = TestElevated $scriptDir;
Write-Output "`t- Check elevated PowerShell:`t$test";
If (!$test) { DefaultError; }

$test = TestOS $scriptDir;
Write-Output "`t- Check operating system:`t$test";
If (!$test) { DefaultError; }

$test = TestNetwork $scriptDir;
Write-Output "`t- Check network connection:`t$test";
If (!$test) { DefaultError; }

$test = TestStaticIP $scriptDir;
Write-Output "`t- Check static IP address:`t$test";
If (!$test) { DefaultError; }

# Test windows features and roles
Write-Output "`nAdd Windows roles and features:`n";
Write-Output ("`t- Install .NET Framework 3.5 Features:`t`t`t`t" + (Add-WindowsFeature NET-Framework-Features).ExitCode);
Write-Output ("`t- Install Web Server (IIS):`t`t`t`t`t" + (Add-WindowsFeature Web-Server).ExitCode);
Write-Output ("`t- Install IIS Management Scripts and Tools:`t`t`t" + (Add-WindowsFeature Web-Scripting-Tools).ExitCode);
#Write-Output ("`t- Install ISAPI Extensions:`t`t`t`t`t" + (Add-WindowsFeature Web-ISAPI-Ext).ExitCode);
#Write-Output ("`t- Install ISAPI Filters:`t`t`t`t`t" + (Add-WindowsFeature Web-ISAPI-Filter).ExitCode);
#Write-Output ("`t- Install .NET Extensibility 4.5:`t`t`t`t" + (Add-WindowsFeature Web-Net-Ext45).ExitCode);
#Write-Output ("`t- Install ASP.NET 4.5:`t`t`t`t`t`t" + (Add-WindowsFeature Web-Asp-Net45).ExitCode);
Write-Output ("`t- Install CGI:`t`t`t`t`t`t`t" + (Add-WindowsFeature Web-CGI).ExitCode);
Write-Output ("`t- Install Windows Authentication:`t`t`t`t" + (Add-WindowsFeature Web-Windows-Auth).ExitCode);
Write-Output ("`t- Install Active Directory module for Windows PowerShell:`t" + (Add-WindowsFeature RSAT-AD-PowerShell).ExitCode);
Write-Output ("`t- Install DNS Server Tools:`t`t`t`t`t" + (Add-WindowsFeature RSAT-DNS-Server).ExitCode);

# Include relevent modules 
Import-Module ServerManager;
Import-Module WebAdministration;

# Test domain
Write-Output "`nDomain Requirements:`n";

$test = TestDomain $scriptDir;
Write-Output "`t- Check machine is a member of a domain:`t$test";
If (!$test) { DefaultError; }

$test = TestDomainAdmin $scriptDir;
Write-Output "`t- Check running as domain administrator:`t$test";
If (!$test) { DefaultError; }

# Test software requirements
Write-Output "`nSoftware Requirements:`n";

$test = TestPHP $scriptDir;
Write-Output "`t- Check PHP 5.6 x86:`t`t`t`t`t`t`t$test";
If (!$test) { DefaultError; }

$test = TestSoftware $scriptDir "MySQL Server";
Write-Output "`t- Check MySQL:`t`t`t`t`t`t`t`t$test";
If (!$test) { DefaultError; }

$test = TestSoftware $scriptDir "MySQL Connector Net";
Write-Output "`t- Check MySQL Connector/Net:`t`t`t`t`t`t$test";
If (!$test) { DefaultError; }

$test = TestSoftware $scriptDir "Microsoft Visual C++ 2012 x86";
Write-Output "`t- Check Visual C++ Redistributable for Visual Studio 2012 Update 4 x86:`t$test";
If (!$test) { DefaultError; }

# Begin Configuration
Write-Output "`nConfiguration:";

$test = ConfigMySQL $scriptDir;
Write-Output ("`t- Configure MySQL: " + $test);
If (!$test) { DefaultError; }

$return = ConfigAdminAccount $scriptDir;
Write-Output ("`n`t- Choose an administrative account: " + $return.cred.UserName);
If (!$return.success) { DefaultError; }

$test = ConfigAppPool $scriptDir $return.cred;
Write-Output ("`t- Create IIS application pool: " + $test);
If (!$test) { DefaultError; }

$test = ConfigWebSite $scriptDir;
Write-Output ("`t- Create IIS web site: " + $test);
If (!$test) { DefaultError; }

$test = ConfigDNS $scriptDir;
Write-Output ("`t- Create DNS record: " + $test);
If (!$test) { DefaultError; }

$test = ConfigScheduledTask $scriptDir $return.cred;
Write-Output ("`t- Configure scheduled tasks: " + $test);
If (!$test) { DefaultError; }

Write-Host "`nInstallation complete.`n";
Write-Host "NOTE: You can delete the files and folders in this directory. They are safe in the IIS directory you chose.";
Write-Host "NOTE: Depending on your system, you will need to use Set-ExecutionPolicy or sign all the powershell scripts.";

# SIG # Begin signature block
# MIIIXQYJKoZIhvcNAQcCoIIITjCCCEoCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUPBbqgHkNALd4dFiDBC0jLdKz
# If+gggXFMIIFwTCCBKmgAwIBAgIKO4FEZwAAAAAMYzANBgkqhkiG9w0BAQUFADBT
# MRUwEwYKCZImiZPyLGQBGRYFbG9jYWwxGjAYBgoJkiaJk/IsZAEZFgpjbGVhcndh
# dGVyMR4wHAYDVQQDExVjbGVhcndhdGVyLUNXLURDMDEtQ0EwHhcNMTUxMTMwMTc0
# NjAzWhcNMTYxMTI5MTc0NjAzWjB0MRUwEwYKCZImiZPyLGQBGRYFbG9jYWwxGjAY
# BgoJkiaJk/IsZAEZFgpjbGVhcndhdGVyMRkwFwYDVQQLExBDbGVhcndhdGVyIFVz
# ZXJzMQ0wCwYDVQQLEwRUSU1TMRUwEwYDVQQDEwxJc2FhY00uYWRtaW4wggEiMA0G
# CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC8zeRq/JFNf1nUuVIaXIyfZRrjda4x
# JCPMM7V10PzxI04V9b5/po6CSLleIGzbLRp7EygTaeSuxUjV2BOiY8sdqReKzDpR
# HCYqafoGLNPXilkkVK7N7+DfKEvxxe2onMOF9+J12Qq+Vx5C3GzuXTLWY2QhOB0J
# +rqp+0bSELHZ5W46NPElRR17JTFCgt4f5OPLXVRglkWGLWq7VEQB6v8QJ6CB7qUZ
# 22qxglENzYkSTnNoAZukeVsn+xDoNKwbaxzhZ7jd4Llr+k1/JVHY6HrJ4KxJp5/T
# VPWIr1Uo3x2tGpavRrSjYrGQ5O7TfK6xjmZu2hZzm4Pi7FZWvhsMZiGnAgMBAAGj
# ggJ0MIICcDAlBgkrBgEEAYI3FAIEGB4WAEMAbwBkAGUAUwBpAGcAbgBpAG4AZzAT
# BgNVHSUEDDAKBggrBgEFBQcDAzAOBgNVHQ8BAf8EBAMCB4AwHQYDVR0OBBYEFELc
# ts9M6zKWtU/1h4X1RBdqFs34MB8GA1UdIwQYMBaAFLyn4uwnfdhI5sBmtQ6PO/iw
# N4jbMIHYBgNVHR8EgdAwgc0wgcqggceggcSGgcFsZGFwOi8vL0NOPWNsZWFyd2F0
# ZXItQ1ctREMwMS1DQSxDTj1jdy1kYzAxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXkl
# MjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPWNsZWFy
# d2F0ZXIsREM9bG9jYWw/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29i
# amVjdENsYXNzPWNSTERpc3RyaWJ1dGlvblBvaW50MIHMBggrBgEFBQcBAQSBvzCB
# vDCBuQYIKwYBBQUHMAKGgaxsZGFwOi8vL0NOPWNsZWFyd2F0ZXItQ1ctREMwMS1D
# QSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMs
# Q049Q29uZmlndXJhdGlvbixEQz1jbGVhcndhdGVyLERDPWxvY2FsP2NBQ2VydGlm
# aWNhdGU/YmFzZT9vYmplY3RDbGFzcz1jZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MDgG
# A1UdEQQxMC+gLQYKKwYBBAGCNxQCA6AfDB1pc2FhY20uYWRtaW5AY2xlYXJ3YXRl
# ci5sb2NhbDANBgkqhkiG9w0BAQUFAAOCAQEAKNs9fFpoMwN87TTxRu6RCtfOFvt6
# lkmwNr1KGASv3sSqBkz7DE+3e99bhJpqCcpyrr5KSbgluMwTXPI3LmHDXb2CDblK
# yLRfFVfXXK6Im6+oAaxGZcEJhYZbDrDSKPBGEFwnt60flWO/MYCbdALRLiv5Moa1
# hHhbeGraiOnbzCULSA2QT8HlOA6k//gXloVysFGws2BcpjqeiNjiNcN492aHgFyq
# w646IIAQX69DCtNII+MxwGmC2hdCJ7V2b3ackQHuelnspI7lPy/mJO/eY+aFDUK4
# TtFoVFio7pLbsCh34pByvpktaLh4s8ZQUGCShG5va5u/4C06VppQQ3Jb/jGCAgIw
# ggH+AgEBMGEwUzEVMBMGCgmSJomT8ixkARkWBWxvY2FsMRowGAYKCZImiZPyLGQB
# GRYKY2xlYXJ3YXRlcjEeMBwGA1UEAxMVY2xlYXJ3YXRlci1DVy1EQzAxLUNBAgo7
# gURnAAAAAAxjMAkGBSsOAwIaBQCgeDAYBgorBgEEAYI3AgEMMQowCKACgAChAoAA
# MBkGCSqGSIb3DQEJAzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
# BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBTzzAvIyA9FFIkfoSWy2/Lj5CSt3TAN
# BgkqhkiG9w0BAQEFAASCAQCh3eoQDdiQJOFwVkmLZvkaIUBW95vdOtDHl0eqB3St
# fnANOGAPpZZstv+6ycFBh69gam+AMHPPNuksKThLCMoBV8ImI6tVK+0S6wnJyAyd
# XZjlmugn7KOToQrR7nS+PFskSTZ0AQgVnb/BRxl3qA3mlUYbtnDuqplWx5UzZxoP
# MeX0PB6W8AN/oEHQ0dLtV17500iIf9BWX9EoxpUF0mYAe5fZjRzl7GTGEigwpoKR
# XsCOUGvDlc/ttAloraBsNudJQIReN/kMztXsvYf3wTehAcT2faolojoeCRIRqAGr
# ZO0Yz0X99HlKgGksD49zfiBb8ARhPg44pL4SxvGShAmR
# SIG # End signature block