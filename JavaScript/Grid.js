Grid = function () {
  this.widgets = [];
  this.machines = [];
};

Grid.prototype = {
  constructor: Grid,

  selectWidgets: function () {
    $.ajax({
      method: "GET",
      url: "PHP/SelectWidgets.php?userid=" + globalUser.userID,
      async: false,
      dataType: "xml",
      context: this,
      success: function (result) {
        xml = result.getElementsByTagName("widget");

        for (var i = 0; i < xml.length; i++) {
          var widget = this.createWidget(xml[i].getElementsByTagName("type")[0].childNodes[0].nodeValue);
          widget.widgetID = xml[i].getElementsByTagName("widgetid")[0].childNodes[0].nodeValue;
          widget.sizeX = xml[i].getElementsByTagName("sizex")[0].childNodes[0].nodeValue;
          widget.sizeY = xml[i].getElementsByTagName("sizey")[0].childNodes[0].nodeValue;
          widget.col = xml[i].getElementsByTagName("col")[0].childNodes[0].nodeValue;
          widget.row = xml[i].getElementsByTagName("row")[0].childNodes[0].nodeValue;
          this.constructWidget(widget);
        }
      }
    });
  },

  allowDrop: function (e) {
    e.preventDefault();
  },

  drag: function (e) {
    e.dataTransfer.setData("text", e.target.id);
  },

  drop: function (e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    var type = e.dataTransfer.getData("text");
    var widget = this.createWidget(type);

    $.ajax({
      method: "POST",
      url: "PHP/InsertWidget.php",
      data: "userid=" + globalUser.userID,
      async: false,
      dataType: "text",
      success: function (result) {
        widget.widgetID = result.trim();
      }
    });

    this.constructWidget(widget);
    this.updateWidget(widget);
  },

  createWidget: function (type) {
    return (type == "aScripts") ? new WidgetScripts(type) : new WidgetMachines(type);
  },

  updateWidget: function (widget) {
    $.ajax({
      method: "POST",
      url: "PHP/UpdateWidget.php",
      data: "widgetid=" + widget.widgetID + "&type=" + widget.type + "&sizex=" + widget.sizeX + "&sizey=" + widget.sizeY + "&col=" + widget.col + "&row=" + widget.row + "&userid=" + globalUser.userID
    });
  },

  constructWidget: function (widget) {
    widget.setup();
    var html = "<li id='" + widget.widgetID + "' class='gs_w' ondblclick='globalGrid.deconstructWidget(" + widget.widgetID + ")' style='overflow: hidden;' data-min-sizex='" + widget.minSizeX + "' data-min-sizey='" + widget.minSizeY + "'>" + widget.getReturn() + "</li>";
    var gridsterWidget = globalGridster.add_widget(
        html, widget.sizeX, widget.sizeY,
        widget.col, widget.row);
    $(document.getElementById(widget.widgetID)).foundation();

    widget.col = gridsterWidget[0].dataset.col;
    widget.row = gridsterWidget[0].dataset.row;
    this.widgets[widget.widgetID] = widget;
  },

  reconstructWidget: function (widget) {
    widget.setup();
    document.getElementById(widget.widgetID).innerHTML = widget.getReturn();
    $(document.getElementById(widget.widgetID)).foundation();
  },

  deconstructWidget: function (id) {
    var widget = document.getElementById(id);
    this.widgets[id] = undefined;
    globalGridster.remove_widget(widget);

    $.ajax({
      method: "POST",
      url: "PHP/SoftDeleteWidget.php",
      data: "widgetid=" + id
    });
  },

  selectMachines: function () {
    $.ajax({
      method: "GET",
      url: "PHP/SelectMachines.php?userid=" + globalUser.userID,
      async: false,
      dataType: "xml",
      context: this,
      success: function (result) {
        var xml = result.getElementsByTagName("machine");
        this.machines.length = 0;

        for (var i = 0; i < xml.length; i++) {
          this.machines.push({
            machineid: xml[i].getElementsByTagName("machineid")[0].childNodes[0].nodeValue,
            address: xml[i].getElementsByTagName("address")[0].childNodes[0].nodeValue,
            hostname: xml[i].getElementsByTagName("hostname")[0].childNodes[0].nodeValue,
            ping: xml[i].getElementsByTagName("ping")[0].childNodes[0].nodeValue,
            favorite: xml[i].getElementsByTagName("favorite")[0].childNodes[0].nodeValue,
            critical: xml[i].getElementsByTagName("critical")[0].childNodes[0].nodeValue,
            error: xml[i].getElementsByTagName("error")[0].childNodes[0].nodeValue,
            warning: xml[i].getElementsByTagName("warning")[0].childNodes[0].nodeValue
          });
        }
      }
    });
  }
};