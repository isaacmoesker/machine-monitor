WidgetMachines = function (type) {
  this.type = type;
  this.output = [];
  this.widgetID;
  this.sizeX = 3;
  this.sizeY = 4;
  this.col = 1;
  this.row = 1;
  this.minSizeX = 1;
  this.minSizeY = 1;
  this.sortPingClass = "fa fa-sort";
  this.sortHostnameClass = "fa fa-sort";
};

WidgetMachines.prototype = {
  constructor: WidgetMachines,

  setup: function () {
    this.output = [];

    switch (this.type) {
      case "aAll":
        var favorite = 0;
        var star = "fa fa-star-o fa-lg fa-fw";
        break;
      case "aFav":
        var favorite = 1;
        var star = "fa fa-star fa-lg fa-fw";
        break;
    }

    var j = 0;
    var positionClass = "dropdown-pane";
    for (var i = 0; i < globalGrid.machines.length; i++) {
      if (favorite == globalGrid.machines[i].favorite) {
        if (j > 3) {
          positionClass = "dropdown-pane top";
        } else {
          j++;
        }
        this.output.push(this.generateRows(i, star, positionClass));
      }
    }
  },

  generateRows: function (i, star, positionClass) {
    var address = globalGrid.machines[i].address;
    var hostname = globalGrid.machines[i].hostname;
    var ping = globalGrid.machines[i].ping;
    var critical = globalGrid.machines[i].critical;
    var error = globalGrid.machines[i].error;
    var warning = globalGrid.machines[i].warning;

    if (warning > 0) { var warningColour = "alert"; }
    else { var warningColour = "success"; }
    if (critical > 0) { var criticalColour = "alert"; }
    else { var criticalColour = "success"; }
    if (error > 0) { var errorColour = "alert"; }
    else { var errorColour = "success"; }

    if (ping == 1) {
      var style = "color: #3adb76;";
      var eventCounts = "<a onclick='globalGrid.widgets[" + this.widgetID + "].getEvents(\"" + hostname + "\",3)'>" +
                          "<span class='label " + warningColour + "' style='width: 100%; text-align:center;'>Warnings: " + warning + "</span>" +
                        "</a><br />" +
                        "<a onclick='globalGrid.widgets[" + this.widgetID + "].getEvents(\"" + hostname + "\",1)'>" +
                          "<span class='label " + criticalColour + "' style='width: 100%; text-align:center;'>Criticals: " + critical + "</span>" +
                        "</a><br />" +
                        "<a onclick='globalGrid.widgets[" + this.widgetID + "].getEvents(\"" + hostname + "\",2)'>" +
                          "<span class='label " + errorColour + "' style='width: 100%; text-align:center;'>Errors: " + error + "</span>" +
                        "</a>";
    } else {
      var style = "color: #ec5840;";
      var eventCounts = "<a><span class='label " + warningColour + "' style='width:100%; text-align:center;'>Warnings: " + warning + "</span></a><br />" +
                        "<a><span class='label " + criticalColour + "' style='width:100%; text-align:center;'>Criticals: " + critical + "</span></a><br />" +
                        "<a><span class='label " + errorColour + "' style='width:100%; text-align:center;'>Errors: " + error + "</span></a>";
    }

    var shortHost = this.widgetID + hostname.split(".", 1);
    return "<tr><td style='text-align:center'>" +
             "<i class='fa fa-circle fa-lg fa-fw' style='" + style + "'></i>" +
           "</td><td data-toggle='" + shortHost + "' style='word-break:break-all;'>" +
             hostname +
             "<div id='" + shortHost + "' class='" + positionClass + "' data-dropdown data-hover='true'>" +
               "<table class='hover' style='margin-bottom:unset'><tr align='center'><td>" +
                 address +
               "</td></tr><tr align='center'><td>" +
                 eventCounts +
               "</td></tr><tr align='center'><td>" +
                 "<a class='" + star + "' style='color: #333333;' onclick=\"globalGrid.widgets[" + this.widgetID + "].updateFavorite(" + i + ")\"></a>&nbsp;" +
                 "<a onclick=\"globalGrid.widgets[" + this.widgetID + "].launchRDP('" + address + "')\" class='fa fa-desktop fa-lg fa-fw' style='color: #333333;'></a>" +
               "</td></tr></table>" +
             "</div>" +
           "</td></tr>";
  },

  getReturn: function () {
    var out = "";
    for (var i = 0; i < this.output.length; i++) {
      out += this.output[i];
    }

    return "<div id='" + this.widgetID + this.type + "' style='height:100%; overflow:scroll;'>" +
           "<table style='margin-bottom:10px; table-layout:fixed;' class='hover'>" +
             "<col style='width:70px'>" +
             "<col style='width:auto'>" +
             "<thead>" +
               "<tr>" +
                 "<th style='text-align:center'>Ping <a id='" + this.widgetID + "aSortPing' onclick='globalGrid.widgets[" + this.widgetID + "].sortPing(\"" + this.widgetID + "aSortPing\")' class='" + this.sortPingClass + "' style='color: #333333' /></th>" +
                 "<th>Host Name <a id='" + this.widgetID + "aSortHostname' onclick='globalGrid.widgets[" + this.widgetID + "].sortHostname(\"" + this.widgetID + "aSortHostname\")' class='" + this.sortHostnameClass + "' style='color: #333333' /></th>" +
               "</tr>" +
             "</thead>" +
             "<tbody id='" + this.widgetID + "tbody" + this.type + "'>" +
               out +
             "</tbody>" +
           "</table>" +
         "</div>";
  },

  sortHostname: function (id) {
    if (this.sortHostnameClass != "fa fa-sort-asc") {
      globalGrid.machines.sort(function (a, b) {
        return (a.hostname > b.hostname) ? 1 : ((b.hostname > a.hostname) ? -1 : 0);
      }
      );
      this.sortHostnameClass = "fa fa-sort-asc";
    } else {
      globalGrid.machines.sort(function (b, a) {
        return (a.hostname > b.hostname) ? 1 : ((b.hostname > a.hostname) ? -1 : 0);
      }
      );
      this.sortHostnameClass = "fa fa-sort-desc";
    }
    this.sortPingClass = "fa fa-sort";
    globalGrid.reconstructWidget(this);
  },

  sortPing: function (id) {
    if (this.sortPingClass != "fa fa-sort-asc") {
      globalGrid.machines.sort(function (a, b) {
        return (a.ping > b.ping) ? 1 : ((b.ping > a.ping) ? -1 : 0);
      }
      );
      this.sortPingClass = "fa fa-sort-asc";
    } else {
      globalGrid.machines.sort(function (b, a) {
        return (a.ping > b.ping) ? 1 : ((b.ping > a.ping) ? -1 : 0);
      }
      );
      this.sortPingClass = "fa fa-sort-desc";
    }
    this.sortHostnameClass = "fa fa-sort";
    globalGrid.reconstructWidget(this);
  },

  updateFavorite: function (i) {
    switch (globalGrid.machines[i].favorite) {
      case "0":
        $.ajax({
          method: "POST",
          url: "PHP/InsertFavorite.php",
          data: "userid=" + globalUser.userID + "&machineid=" + globalGrid.machines[i].machineid,
          async: false
        });
        globalGrid.machines[i].favorite = "1";
        break;
      case "1":
        $.ajax({
          method: "POST",
          url: "PHP/DeleteFavorite.php",
          data: "userid=" + globalUser.userID + "&machineid=" + globalGrid.machines[i].machineid,
          async: false
        });
        globalGrid.machines[i].favorite = "0";
        break;
    }
    for (var j in globalGrid.widgets) {
      globalGrid.reconstructWidget(globalGrid.widgets[j]);
    }
  },

  launchRDP: function (address) {
    try {
      var ws = new ActiveXObject("WScript.Shell");
      ws.Exec("mstsc /v:" + address);
    }
    catch (err) {
      var output = "<h2>Browser Error</h2><p><strong>Incompatible Browser</strong><br />" +
                   "RDP requires Internet Explorer.<br /><br />" +
                   "<strong>ActiveX Options</strong><br />" +
                   "Navigate to 'Internet Options > Security > Local Intranet > Custom Level > ActiveX controls and plugins'. " +
                   "Set 'Initialize and script ActiveX controls not marked as safe for scripting' to 'Prompt'.</p>" +
                   "<button class='close-button' data-close aria-label='Close modal' type='button'>" +
                     "<span aria-hidden='true' class='fa fa-times'></span>" +
                   "</button>";

      $('#modal').html(output).foundation('open');
    }
  },

  getEvents: function (hostname, level) {
    $.ajax({
      method: "GET",
      url: "PHP/GetEvents.php?hostname=" + hostname + "&level=" + level,
      dataType: "xml",
      success: function (result) {
        if (result != null) {
          var xml = result.getElementsByTagName("event");

          var title;
          switch (level) {
            case 1: title = "Criticals"; break;
            case 2: title = "Errors"; break;
            case 3: title = "Warnings"; break;
          }

          var output = "<h2>" + title + "</h2><table width='100%'><thead><tr>" +
                         "<th width='5%'>Log</th>" +
                         "<th width='5%'>ID</th>" +
                         "<th width='70%'>Message</th>" +
                         "<th width='20%'>Time Created</th>" +
                       "</tr></thead><tbody id='tbodyMachines'>";

          for (var i = 0; i < xml.length; i++) {
            var Message = xml[i].getElementsByTagName("Message")[0].childNodes[0].nodeValue;
            var Id = xml[i].getElementsByTagName("Id")[0].childNodes[0].nodeValue;
            var LogName = xml[i].getElementsByTagName("LogName")[0].childNodes[0].nodeValue;
            var TimeCreated = xml[i].getElementsByTagName("TimeCreated")[0].childNodes[0].nodeValue;

            output += "<tr><td>" + LogName +
                      "</td><td>" + Id +
                      "</td><td>" + Message +
                      "</td><td>" + TimeCreated +
                      "</td></tr>";
          }

          output += "</tbody></table>"
            + "<button class='close-button' data-close aria-label='Close modal' type='button'>"
              + "<span aria-hidden='true' class='fa fa-times'></span>"
            + "</button>";

          $('#modal').html(output).foundation('open');
        }
      }
    });
  }
};