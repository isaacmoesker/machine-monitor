WidgetScripts = function (type) {
  this.type = type;
  this.output;
  this.widgetID;
  this.sizeX = 3;
  this.sizeY = 4;
  this.col = 1;
  this.row = 1;
  this.minSizeX = 1;
  this.minSizeY = 1;
  this.sortScriptsClass = "fa fa-sort";
};

WidgetScripts.prototype = {
  constructor: WidgetScripts,

  setup: function() {

  },

  getReturn: function () {
    var scriptTitles = "";
    for (var i = 0; i < globalScripts.length; i++) {
      scriptTitles += "<tr><td data-toggle='scriptID" + globalScripts[i].scriptID + "'>" + globalScripts[i].title +
        "<div class='dropdown-pane' id='scriptID" + globalScripts[i].scriptID + "' data-dropdown style='padding:10px'>" +
          "Do you want to run this script?" +
          "<div class='expanded button-group' style='margin:unset'>" +
            "<input type='button' class='button alert' value='No'>" +
            "<input type='button' class='button success' onclick=\"globalScripts[" + i + "].runScript()\" value='Yes'>" +
          "</div>" +
        "</div></td></tr>";
    }

    return "<div id='" + this.widgetID + "divScripts' style='height:100%; overflow:scroll;'>" +
             "<table style='margin-bottom:10px; table-layout:fixed;' class='hover'>" +
               "<col style='width:100%;'>" +
               "<thead>" +
                 "<tr>" +
                   "<th>Scripts <a id='" + this.widgetID + "aSortScripts' onclick='globalGrid.widgets[" + this.widgetID + "].sortScripts(\""+ this.widgetID + "aSortScripts\")' class='" + this.sortScriptsClass + "' style='color: #333333' /></th>" +
                 "</tr>" +
               "</thead>" +
               "<tbody id='" + this.widgetID + "tbodyScripts'>" +
                 scriptTitles +
               "</tbody>" +
             "</table>" +
           "</div>";
  },

  sortScripts: function (id) {
    if (this.sortScriptsClass != "fa fa-sort-asc") {
      globalScripts.sort(function (a, b) {
        return (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0);
      }
      );
      this.sortScriptsClass = "fa fa-sort-asc";
    } else {
      globalScripts.sort(function (b, a) {
        return (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0);
      }
      );
      this.sortScriptsClass = "fa fa-sort-desc";
    }
    globalGrid.reconstructWidget(this);
  }
};