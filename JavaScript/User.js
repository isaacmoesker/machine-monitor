User = function () {
  this.userID;
  this.gridColor;

  $.ajax({
    method: "GET",
    url: "PHP/SelectInsertUser.php",
    async: false,
    dataType: "xml",
    context: this,
    success: function (result) {
      var xml = result.getElementsByTagName("user")[0];

      this.userID = xml.getElementsByTagName("userid")[0].childNodes[0].nodeValue;
      this.gridColor = xml.getElementsByTagName("gridcolor")[0].childNodes[0].nodeValue;

      if (this.userID === undefined) {
        alert("The variables userid and gridcolor could not be retrieved. Do not continue.");
      } else {
        this.changeBackground();
      }
    }
  });
};

User.prototype = {
  constructor: User,

  update: function () {
    $.ajax({
      method: "POST",
      url: "PHP/UpdateUser.php",
      data: "userid=" + this.userID + "&gridcolor=" + this.gridColor,
      async: true
    });
  },

  changeBackground: function () {
    document.body.style.backgroundColor = this.mapColor(this.gridColor);
  },

  mapColor: function (color) {
    switch (color.toLowerCase()) {
      case "#2199e8": return "primary"; break;
      case "#777": case "#777777": return "secondary"; break;
      case "#3adb76": return "success"; break;
      case "#ffae00": return "warning"; break;
      case "#ec5840": return "alert"; break;

      case "primary": return "#2199e8"; break;
      case "secondary": return "#777777"; break;
      case "success": return "#3adb76"; break;
      case "warning": return "#ffae00"; break;
      case "alert": return "#ec5840"; break;

      default: return color;
    }
  }
};