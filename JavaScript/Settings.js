function Settings() {
  this.colour;
  this.taskDelay;
};

Settings.prototype = {
  constructor: Settings,

  open: function () {
    var scriptRows = "";
    if (globalScripts.length > 0) {
      for (var i = 0; i < globalScripts.length; i++) { // onclick=\"globalSettings.removeScript(" + i + ")\"
        scriptRows += "<tr>" +
          "<td style='text-align:center'>" +
            "<a class='fa fa-trash fa-fw fa-lg' data-toggle='dropdown" + i + "' aria-hidden='true' />" +
            "<a class='fa fa-pencil fa-fw fa-lg' onclick=\"globalSettings.editScript(" + i + ")\" aria-hidden='true' />" +
          "</td><td>" + globalScripts[i].title + 
          "<div class='dropdown-pane' id='dropdown" + i + "' data-dropdown style='padding:10px'>" +
            "Do you want to remove this script?" +
            "<div class='expanded small button-group' style='margin:unset'>" +
              "<input type='button' class='button alert' data-toggle='dropdown" + i + "' value='No'>" +
              "<input type='button' class='button success' onclick=\"globalSettings.removeScript(" + i + ")\" value='Yes'>" +
            "</div>" +
          "</div></td></tr>";
      }
    }
    else {
      scriptRows = "<tr><td colspan='2'>No scripts <i class='fa fa-frown-o' aria-hidden='true' /></td></tr>";
    }

    var output = "<h2>Settings</h2>" +
      "<fieldset class='fieldset'>" +
        "<legend><strong>Grid Color</strong></legend>" +
        "<div class='input-group'>" +
          "<div class='input-group-button'>" +
            "<input id='settingsBtnPrimary' type='button' class='button primary expanded' onclick=\"globalSettings.setColor('primary', 'settingsBtnPrimary')\">" +
          "</div>" +
          "<div class='input-group-button'>" +
            "<input id='settingsBtnSecondary' type='button' class='button secondary expanded' onclick=\"globalSettings.setColor('secondary', 'settingsBtnSecondary')\">" +
          "</div>" +
          "<div class='input-group-button'>" +
            "<input id='settingsBtnSuccess' type='button' class='button success expanded' onclick=\"globalSettings.setColor('success', 'settingsBtnSuccess')\">" +
          "</div>" +
          "<div class='input-group-button'>" +
            "<input id='settingsBtnWarning' type='button' class='button warning expanded' onclick=\"globalSettings.setColor('warning', 'settingsBtnWarning')\">" +
          "</div>" +
          "<div class='input-group-button'>" +
            "<input id='settingsBtnAlert' type='button' class='button alert expanded' onclick=\"globalSettings.setColor('alert', 'settingsBtnAlert')\">" +
          "</div>" +
          "<span class='input-group-label'><span id='settingLblColor' class='label " + globalUser.gridColor + "'>Selected</span></span>" +
        "</div>" +
      "</fieldset>" +
      "<fieldset class='fieldset'>" +
        "<legend><strong>Script Invintory</strong></legend>" +
        "<ul class='accordion' data-accordion style='padding-left:0px'>" +
          "<li class='accordion-item' data-accordion-item>" +
            "<a href='#' class='accordion-title'>New Script</a>" +
            "<div class='accordion-content' data-tab-content>" +
              "<label>Title" +
                "<input id='scriptTitle' type='text'>" +
              "</label>" +
              "<label>Script" +
                "<textarea id='scriptData' rows='4'></textarea>" +
              "</label>" +
              "<div class='expanded button-group'>" +
                "<input type='button' class='button alert' onclick=\"$('#scriptTitle,#scriptData').val('')\" value='Reset'>" +
                "<input type='button' class='button success' onclick=\"globalSettings.createScript($('#scriptTitle').val(),$('#scriptData').val())\" value='Submit'>" +
              "</div>" +
            "</div>" +
          "</li>" +
          "<li class='accordion-item is-active' data-accordion-item>" +
            "<a href='#' class='accordion-title'>Script Inventory</a>" +
            "<div class='accordion-content' data-tab-content>" +
              "<div class='table-scroll'>" +
                "<table class='hover' style='width:100%;height:50px;'>" +
                  "<col style='width:70px'>" +
                  "<col style='width:auto'>" +
                  "<thead>" +
                    "<tr>" +
                      "<th>Actions</th>" +
                      "<th>Title</th>" +
                    "</tr>" +
                  "</thead>" +
                  "<tbody>" +
                    scriptRows +
                  "</tbody>" +
                "</table>" +
              "</div>" +
            "</div>" +
          "</li>" +
        "</ul>" +
      "</fieldset>" +
      "<button class='close-button' data-close aria-label='Close modal' type='button'>" +
        "<span aria-hidden='true' class='fa fa-times'></span>" +
      "</button>";

    $("#modal").html(output).foundation("open");
    $(".accordion").foundation();
  },

  setColor: function (color, buttonId) {
    globalUser.gridColor = color;
    globalUser.update();
    globalUser.changeBackground();
    $("#settingLblColor").removeClass().addClass("label " + color);
  },

  createScript: function (title, data) {
    if (title !== undefined || data !== undefined) {
      var index = globalScripts.push(
        new Script(
          undefined,
          globalUser.userID,
          title,
          data
        )
      );
      globalScripts[index - 1].insert();
      this.open();
    }
  },

  editScript: function (index) {
    var output = "<h2>Settings</h2>" +
      "<fieldset class='fieldset'>" +
        "<legend><strong>Edit Script</strong></legend>" +
        "<label>Title" +
          "<input id='editScriptTitle' type='text'>" +
        "</label>" +
        "<label>Script" +
          "<textarea id='editScriptData' rows='10'></textarea>" +
        "</label>" +
        "<div class='expanded button-group'>" +
          "<input type='button' class='button alert' onclick=\"$('#editScriptTitle,#editScriptData').val('')\" value='Reset'>" +
          "<input type='button' class='button success' onclick=\"globalSettings.updateScript(" + index + ",$('#editScriptTitle').val(),$('#editScriptData').val())\" value='Submit'>" +
        "</div>" +
      "</fieldset>" +
      "<button class='close-button' data-close aria-label='Close modal' type='button'>" +
        "<span aria-hidden='true' class='fa fa-times'></span>" +
      "</button>";

    $("#modal").html(output).foundation("open");
    $("#editScriptTitle").val(globalScripts[index].title);
    $("#editScriptData").val(globalScripts[index].data);
  },

  updateScript: function (index, title, data) {
    if (title !== undefined || data !== undefined) {
      globalScripts[index].title = title;
      globalScripts[index].data = data;
      globalScripts[index].update();
      this.open();
    }
  },

  removeScript: function (index) {
    if (index !== undefined) {
      globalScripts[index].remove();
      globalScripts[index] = undefined;
      globalScripts = globalScripts.filter(function (item) { return item !== undefined; });
      this.open();
    }
  }
};
