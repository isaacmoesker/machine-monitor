function Script(scriptID, userID, title, data) {
  this.scriptID = scriptID;
  this.userID = userID;
  this.title = title;
  this.data = data;
};

Script.prototype = {
  constructor: Script,

  insert: function () {
    if (this.proveProperties()) {
      $.ajax({
        method: "POST",
        url: "PHP/InsertScript.php",
        data: "userid=" + this.userID + "&title=" + this.title + "&data=" + this.data,
        dataType: "text",
        context: this,
        success: function (result) {
          this.scriptID = result.trim();
        }
      });
    }
  },

  update: function () {
    if (this.proveProperties() && typeof this.scriptID !== 'undefined') {
      $.ajax({
        method: "POST",
        url: "PHP/UpdateScript.php",
        data: "scriptid=" + this.scriptID + "&userid=" + this.userID + "&title=" + this.title + "&data=" + this.data
      });
    }
  },

  remove: function () {
    if (typeof this.scriptID !== 'undefined') {
      $.ajax({
        method: "POST",
        url: "PHP/SoftDeleteScript.php",
        data: "scriptid=" + this.scriptID
      });
    }
  },

  runScript: function () {
    if (typeof this.scriptID !== 'undefined') {
      $.ajax({
        method: "POST",
        url: "PHP/RunScript.php",
        data: "data=" + this.data,
        success: function (result) {
          console.log(result);
        }
      });
    }
  },

  proveProperties: function () {
    return (typeof this.userID !== 'undefined' && typeof this.title !== 'undefined' && typeof this.data !== 'undefined') ? true : false;
  }
};