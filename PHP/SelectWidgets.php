<?php
require_once "MySQLConnect.php";

try {
  header('Content-Type: text/xml');
  $userid = addslashes($_GET['userid']);
  
  $stmt = $conn->prepare("SELECT widgetid,type,sizex,sizey,col,row FROM Widgets WHERE userid='$userid';");
  $stmt->execute();
  $widgets = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  $t4 = microtime(true);
  
  $output = "<widgets>";
  foreach ($widgets as $widget) {
    $output .= "<widget><widgetid>" . $widget["widgetid"] . "</widgetid>";
    $output .= "<type>" . $widget["type"] . "</type>";
    $output .= "<sizex>" . $widget["sizex"] . "</sizex>";
    $output .= "<sizey>" . $widget["sizey"] . "</sizey>";
    $output .= "<col>" . $widget["col"] . "</col>";
    $output .= "<row>" . $widget["row"] . "</row></widget>";
  }
  $output .= "</widgets>";
  
  echo $output;
}
catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;
?>