<?php
require_once "MySQLConnect.php";

try {
	header('Content-Type: text/xml');
  
	$stmt = $conn->prepare("SELECT * FROM Scripts WHERE title IS NOT NULL;");
	$stmt->execute();
	$scripts = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$output = "<scripts>";
	foreach ($scripts as $script) {
		$output .= "<script><scriptid>" . $script["scriptid"] . "</scriptid>";
		$output .= "<userid>" . $script["userid"] . "</userid>";
		$output .= "<title>" . $script["title"] . "</title>";
		$output .= "<data>" . $script["data"] . "</data></script>";
	}
	$output .= "</scripts>";

	echo $output;
}
catch(PDOException $e) {
	echo "Error: " . $e->getMessage();
}

$conn = null;
?>
