<?php
require_once "MySQLConnect.php";

try {
  $query = "UPDATE Widgets "
    . "SET type=NULL,userid=NULL,sizex=NULL,sizey=NULL,col=NULL,row=NULL "
    . "WHERE widgetid=" . $_POST["widgetid"];
  $stmt = $conn->prepare($query);
  $stmt->execute();
}
catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;
?>