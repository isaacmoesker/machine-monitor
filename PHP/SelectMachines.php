<?php
require_once "MySQLConnect.php";

try {
  header('Content-Type: text/xml');
  $userid = addslashes($_GET["userid"]);

  $stmt = $conn->prepare(
      "SELECT Machines.machineid, Machines.address, Machines.hostname, Machines.ping, "
    . "IF(FavoriteMachines.machineid = Machines.machineid "
    . "AND FavoriteMachines.userid='$userid', 1, 0) AS favorite "
    . "FROM Machines LEFT JOIN FavoriteMachines ON Machines.machineid = FavoriteMachines.machineid "
    . "WHERE Machines.address != '';");
  $stmt->execute();
  $machines = $stmt->fetchAll(PDO::FETCH_ASSOC);

  $stmt = $conn->prepare("SELECT * FROM EventCounts"); 
  $stmt->execute();
  $eventCounts = array_map('reset', $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC));

  $output = "<machines>";
  foreach ($machines as $machine) {
	$ping = ($eventCounts[$machine["machineid"]]["critical"]) ? $machine["ping"] : 0;
	$critical = ($eventCounts[$machine["machineid"]]["critical"]) ? $eventCounts[$machine["machineid"]]["critical"] : 0;
	$error = ($eventCounts[$machine["machineid"]]["error"]) ? $eventCounts[$machine["machineid"]]["error"] : 0;
	$warning = ($eventCounts[$machine["machineid"]]["warning"]) ? $eventCounts[$machine["machineid"]]["warning"] : 0;

    $output .= "<machine><machineid>" . $machine["machineid"] . "</machineid>";
    $output .= "<address>" . $machine["address"] . "</address>";
    $output .= "<hostname>" . $machine["hostname"] . "</hostname>";
    $output .= "<ping>" . $machine["ping"] . "</ping>";
    $output .= "<favorite>" . $machine["favorite"] . "</favorite>";
    $output .= "<critical>" . $critical . "</critical>";
    $output .= "<error>" . $error . "</error>";
    $output .= "<warning>" . $warning . "</warning></machine>";
  }
  $output .= "</machines>";

  echo $output;
}
catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;
?>
