<?php
require_once "MySQLConnect.php";

try {
  header('Content-Type: text/xml');
  $userid = addslashes($_SERVER['LOGON_USER']);

  $query = "SELECT * FROM Users WHERE userid='$userid';";
  $stmt = $conn->prepare($query);
  $stmt->execute();

  if ($stmt->rowCount() === 0) {
	$gridcolor = "primary";
    $query = "INSERT IGNORE INTO Users (userid, gridcolor) VALUES ('$userid', '$gridcolor');";
	$stmt = $conn->prepare($query);
	$stmt->execute();
  } else {
	$user = $stmt->fetch(PDO::FETCH_ASSOC);
	$userid = $user['userid'];
    $gridcolor = $user['gridcolor'];
  }

  echo "<user><userid>$userid</userid><gridcolor>$gridcolor</gridcolor></user>";
}
catch(PDOException $e) {
  echo "<user><userid></userid><gridcolor></gridcolor></user>";
}

$conn = null;
?>
