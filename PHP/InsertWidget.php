<?php
require_once "MySQLConnect.php";

try {
  $userid = addslashes($_POST["userid"]);

  $query = "SELECT MIN(widgetid) AS widgetid FROM Widgets WHERE userid IS NULL;";
  $stmt = $conn->prepare($query);
  $stmt->execute();

  $widgetid = $stmt->fetch(PDO::FETCH_ASSOC);

  if (is_null($widgetid["widgetid"]))  {
    $query = "INSERT INTO Widgets (userid) VALUES ('$userid');";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    echo $conn->lastInsertId();
  }
  else {
    $query = "UPDATE Widgets SET userid='$userid' WHERE widgetid=" . $widgetid['widgetid'] . ";";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    echo $widgetid["widgetid"];
  }
}
catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;
?>
