﻿# Machine Monitor #
***
**README**  
Machine Monitor is an all in one solution for system administrators in need of a domain computer monitoring. The software monitors active directory computers by keeping a machine inventory, ping log, and events. All tools are displayed on a grid and can be moved and resized at will. Windows authentication is used to display each user’s custom view and settings. See project progress at https://trello.com/b/HltzhDwP.
***
**AUTHORS**  
Isaac Moesker  
***
**THANKS**  
Ducksboard, Gridster: http://gridster.net/  
ZURB, Foundation: http://foundation.zurb.com/  
***
**NEWS**  
February 29, 2016  
Tired of setting up Machine Monitor by hand? Done with switching between windows in search of the next installation instruction? Fret not, there is a solution! Introducing the new Machin Monitor installer! Simply run Setup.ps1 and follow the prompts for a truly easy setup experience.  
***
**INSTALL**  
For servers running Windows server 2012 R2:  

* Run Setup.ps1  

For all other Windows operating systems:  

* Install PHP (Tested with PHP 5.6.16 installed via Web Platform Installer)
* If you are receiving “HTTP Error 500.0 - Internal Server Error - The FastCGI process exited unexpectedly”, consider installing Visual C++ Redistributable for Visual Studio 2012 Update 4.
* Install MySQL (Tested with MySQL 5.5 installed via Web Platform Installer)
* Install MySQL Connector/Net (Tested with MySQL Connector Net 6.9.8 installed via Web Platform Installer)
* Add server to your domain
* Assign server a static IP address
* Install Windows roles and features
* .NET Framework 3.5 Features (NET-Framework-Features)
* Web Server (IIS) (Web-Server)
* IIS Management Scripts and Tools (Web-Scripting-Tools)
* CGI (Web-CGI)
* Windows Authentication (Web-Windows-Auth)
* Active Directory module for Windows PowerShell (RSAT-AD-PowerShell)
* DNS Server Tools (RSAT-DNS-Server)
* Run the queries in Documentation\MySQL Setup.txt to setup the database
* Edit uid=root;pwd=P@ssw0rd; in PHP\MySQLConnect.php and PowerShell\MySQLConnect.ps1
* Choose an administrative account
	* Account must be a member of the Domain Administrators group
	* Account must be a member of the Local Administrators group
	* Account member of the 'Log on as batch job' security group
* Create IIS application pool
* Name: Machine Monitor
* Identity: <Administrative Account>
* Create IIS website
	* Name "Machine Monitor"
	* Port: 80 (Recommended if you plan on having a Type A DNS record)
* Add MIME Type in Machine Monitor website
	* File Extension: .woff2
	* MIME Type: font/woff2
* Add Handler Mapping for Machine Monitor website (May already exist, check first)
	* Name: PHP_via_FastCGI
	* Module: FastCgiModule
	* Path: .php
	* Executable <Path the PHP installation>\php-cgi.exe
* Configure Authentication for Machine Monitor website
	* Disable all authentication modes
* Enable Windows Authentication
* Add a Type A DNS record using the static IP address you chose earlier
* Create a folder in Task Scheduler and name it Machine Monitor
* Import all xml files in the Tasks folder into Task Scheduler
* Start InsertMachines task and verify it has completed without errors
* Run “SELECT * FROM Machines” in Machine_Monitor database to verify machines populated successfully.
* Start InsertPing and InsertEventCounts tasks
* All done!
***
**COPYING / LICENSE**  
Yet to be determined  
***
**BUGS**  
If you find something wrong, shoot me an email: isaac.moesker<at>gmail.com